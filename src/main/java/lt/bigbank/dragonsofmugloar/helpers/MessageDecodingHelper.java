package lt.bigbank.dragonsofmugloar.helpers;

import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;

import lt.bigbank.dragonsofmugloar.beans.MessageBean;

public class MessageDecodingHelper {

	private static final Pattern BASE64_PATTERN = Pattern
			.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$");

	public static MessageBean decodeMessage(MessageBean message) {
		checkAndDecodeBase64(message);
		return message;
	}

	private static void checkAndDecodeBase64(MessageBean message) {
		if (BASE64_PATTERN.matcher(message.getMessage()).matches()) {
			message.setAdId(new String(Base64.decodeBase64(message.getAdId())));
			message.setMessage(new String(Base64.decodeBase64(message.getMessage())));
		}
	}

}
