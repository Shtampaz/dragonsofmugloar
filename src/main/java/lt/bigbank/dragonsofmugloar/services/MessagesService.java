package lt.bigbank.dragonsofmugloar.services;

import lt.bigbank.dragonsofmugloar.beans.BaseStatusBean;

public interface MessagesService {

	BaseStatusBean solveSuitableMessage(String gameUrlPath);

}
