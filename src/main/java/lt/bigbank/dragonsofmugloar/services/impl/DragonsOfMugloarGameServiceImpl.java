package lt.bigbank.dragonsofmugloar.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lt.bigbank.dragonsofmugloar.beans.BaseStatusBean;
import lt.bigbank.dragonsofmugloar.beans.GameBean;
import lt.bigbank.dragonsofmugloar.services.GameService;
import lt.bigbank.dragonsofmugloar.services.MessagesService;
import lt.bigbank.dragonsofmugloar.services.ShopService;

/**
 * Main service executing Dragons Of Mugloar game play
 */
@Service
public class DragonsOfMugloarGameServiceImpl implements GameService {

	private final ShopService shopService;

	private final RestTemplate restTemplate;

	private final MessagesService messagesService;

	private final String gameStartPath;

	private static final Logger logger = LoggerFactory.getLogger(DragonsOfMugloarGameServiceImpl.class);

	@Autowired
	public DragonsOfMugloarGameServiceImpl(RestTemplate restTemplate, ShopService shopService,
			MessagesService messagesService, @Value("${dragonsApi.gameStartPath}") String gameStartPath) {
		this.shopService = shopService;
		this.restTemplate = restTemplate;
		this.messagesService = messagesService;
		this.gameStartPath = gameStartPath;
	}

	@Override
	public void play() {
		logger.info("The game is starting!");

		GameBean game = this.restTemplate.postForObject(this.gameStartPath, null, GameBean.class);
		logger.info(game.toString());
		String gameUrlPath = "/" + game.getGameId();

		BaseStatusBean gameStatus = this.messagesService.solveSuitableMessage(gameUrlPath);
		while (gameStatus.getLives() > 0) {
			this.shopService.visitShop(gameUrlPath, gameStatus);
			gameStatus = this.messagesService.solveSuitableMessage(gameUrlPath);
		}

		logger.info("Game over!");
	}

}
