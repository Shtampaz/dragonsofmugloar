package lt.bigbank.dragonsofmugloar.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import lt.bigbank.dragonsofmugloar.beans.BaseStatusBean;
import lt.bigbank.dragonsofmugloar.beans.MessageBean;
import lt.bigbank.dragonsofmugloar.beans.SolvedMessageBean;
import lt.bigbank.dragonsofmugloar.helpers.MessageDecodingHelper;
import lt.bigbank.dragonsofmugloar.selectors.message.MessageSelector;
import lt.bigbank.dragonsofmugloar.services.MessagesService;

/**
 * Service retrieving available messages, selecting which one to solve (based on
 * injected messageSelector) and solving it
 */
@Service
public class MessagesServiceImpl implements MessagesService {

	private final RestTemplate restTemplate;

	private final MessageSelector messageSelector;

	private final String messagesPath;

	private final String solvingPath;

	private static final Logger logger = LoggerFactory.getLogger(MessagesServiceImpl.class);

	@Autowired
	public MessagesServiceImpl(RestTemplate restTemplate, MessageSelector messageSelector,
			@Value("${dragonsApi.messagesPath}") String messagesPath,
			@Value("${dragonsApi.solvingPath}") String solvingPath) {
		this.restTemplate = restTemplate;
		this.messageSelector = messageSelector;
		this.messagesPath = messagesPath;
		this.solvingPath = solvingPath;
	}

	@Override
	public BaseStatusBean solveSuitableMessage(String gameUrlPath) {
		return this.solveSuitableMessage(gameUrlPath, new ArrayList<>());
	}

	private BaseStatusBean solveSuitableMessage(String gameUrlPath, List<String> undecodableMessages) {
		List<MessageBean> possiblySolvableMessages = this.retrieveAllPossiblySolvableMessages(gameUrlPath,
				undecodableMessages);
		if (possiblySolvableMessages.isEmpty()) {
			logger.info("No more solvable messages!");
			BaseStatusBean status = new BaseStatusBean();
			status.setLives(0);
			return status;
		}

		MessageBean messageToSolve = this.messageSelector.selectMessageToSolve(possiblySolvableMessages);
		logger.info(messageToSolve.toString());

		return this.tryToSolveMessage(gameUrlPath, undecodableMessages, messageToSolve);
	}

	private BaseStatusBean tryToSolveMessage(String gameUrlPath, List<String> undecodableMessages,
			MessageBean messageToSolve) {
		try {
			SolvedMessageBean solvedMessage = this.restTemplate.postForObject(
					String.format(this.solvingPath, gameUrlPath, messageToSolve.getAdId()), null,
					SolvedMessageBean.class);
			logger.info(solvedMessage.toString());
			return solvedMessage;
		} catch (HttpStatusCodeException hsce) {
			if (HttpStatus.BAD_REQUEST.equals(hsce.getStatusCode())) {
				logger.info("Unable to solve " + messageToSolve.getAdId() + " message.");
				undecodableMessages.add(messageToSolve.getAdId());
				return this.solveSuitableMessage(gameUrlPath, undecodableMessages);
			}
			throw hsce;
		}
	}

	private List<MessageBean> retrieveAllPossiblySolvableMessages(String gameUrlPath,
			List<String> undecodableMessages) {
		MessageBean[] messages = this.restTemplate.getForObject(String.format(this.messagesPath, gameUrlPath),
				MessageBean[].class);
		this.logAllAvailableMessages(messages);

		List<MessageBean> possiblySolvableMessages = Arrays.asList(messages).stream()
				.map(MessageDecodingHelper::decodeMessage)
				.filter((message) -> !undecodableMessages.contains(message.getAdId())).collect(Collectors.toList());
		return possiblySolvableMessages;
	}

	private void logAllAvailableMessages(MessageBean[] messages) {
		logger.debug("All available messages:");
		for (MessageBean messageBean : messages) {
			logger.debug(messageBean.toString());
		}
	}

}
