package lt.bigbank.dragonsofmugloar.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lt.bigbank.dragonsofmugloar.beans.BaseStatusBean;
import lt.bigbank.dragonsofmugloar.beans.PurchasedItemBean;
import lt.bigbank.dragonsofmugloar.beans.ShopItemBean;
import lt.bigbank.dragonsofmugloar.selectors.shopitem.ShopItemSelector;
import lt.bigbank.dragonsofmugloar.services.ShopService;

/**
 * Service retrieving all available items for purchase and purchasing them based
 * on injected shopItemSelector
 */
@Service
public class ShopServiceImpl implements ShopService {

	private final RestTemplate restTemplate;

	private final ShopItemSelector shopItemSelector;

	private final String shopItemsPath;

	private final String shopItemPurchasePath;

	private static final Logger logger = LoggerFactory.getLogger(ShopServiceImpl.class);

	@Autowired
	public ShopServiceImpl(RestTemplate restTemplate, ShopItemSelector shopItemSelector,
			@Value("${dragonsApi.shopItemsPath}") String shopItemsPath,
			@Value("${dragonsApi.shopItemPurchasePath}") String shopItemPurchasePath) {
		this.restTemplate = restTemplate;
		this.shopItemSelector = shopItemSelector;
		this.shopItemsPath = shopItemsPath;
		this.shopItemPurchasePath = shopItemPurchasePath;
	}

	@Override
	public void visitShop(String gameUrlPath, BaseStatusBean gameStatus) {
		ShopItemBean[] shopItemBeans = restTemplate.getForObject(String.format(this.shopItemsPath, gameUrlPath),
				ShopItemBean[].class);

		this.logAllAvailableItemsToBuy(shopItemBeans);

		this.shopItemSelector.selectItemToBuy(shopItemBeans, gameStatus)
				.ifPresent((shopItem) -> this.buyItem(shopItem, gameUrlPath));
	}

	private void buyItem(ShopItemBean shopItem, String gameUrlPath) {
		logger.info("Buying this item:");
		logger.info(shopItem.toString());
		PurchasedItemBean purchasedItem = restTemplate.postForObject(
				String.format(this.shopItemPurchasePath, gameUrlPath, shopItem.getId()), null, PurchasedItemBean.class);
		logger.info(purchasedItem.toString());
	}

	private void logAllAvailableItemsToBuy(ShopItemBean[] shopItemBeans) {
		logger.debug("Available items to buy:");
		for (ShopItemBean shopItemBean : shopItemBeans) {
			logger.debug(shopItemBean.toString());
		}
	}

}
