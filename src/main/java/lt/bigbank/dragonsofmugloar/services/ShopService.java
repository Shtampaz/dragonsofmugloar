package lt.bigbank.dragonsofmugloar.services;

import lt.bigbank.dragonsofmugloar.beans.BaseStatusBean;

public interface ShopService {

	void visitShop(String gameUrlPath, BaseStatusBean gameStatus);

}
