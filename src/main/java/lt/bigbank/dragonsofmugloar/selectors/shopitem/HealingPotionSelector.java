package lt.bigbank.dragonsofmugloar.selectors.shopitem;

import java.util.Arrays;
import java.util.Optional;

import lt.bigbank.dragonsofmugloar.beans.BaseStatusBean;
import lt.bigbank.dragonsofmugloar.beans.ShopItemBean;
import lt.bigbank.dragonsofmugloar.constants.ShoppingItems;

public class HealingPotionSelector implements ShopItemSelector {

	@Override
	public Optional<ShopItemBean> selectItemToBuy(ShopItemBean[] shopItemBeans, BaseStatusBean gameStatus) {
		return Arrays.stream(shopItemBeans).filter(shopItem -> {
			return ShoppingItems.HEALING_POTION.equals(shopItem.getId()) && gameStatus.getGold() >= shopItem.getCost();
		}).findFirst();
	}

}
