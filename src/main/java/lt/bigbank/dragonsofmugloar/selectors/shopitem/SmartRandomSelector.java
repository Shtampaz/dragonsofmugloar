package lt.bigbank.dragonsofmugloar.selectors.shopitem;

import java.util.Optional;
import java.util.Random;

import lt.bigbank.dragonsofmugloar.beans.BaseStatusBean;
import lt.bigbank.dragonsofmugloar.beans.ShopItemBean;

public class SmartRandomSelector implements ShopItemSelector {

	private static final int MIN_LIVES_FOR_BUYING_NON_HP_ITEMS = 5;

	@Override
	public Optional<ShopItemBean> selectItemToBuy(ShopItemBean[] shopItemBeans, BaseStatusBean gameStatus) {
		if (gameStatus.getLives() < MIN_LIVES_FOR_BUYING_NON_HP_ITEMS) {
			return new HealingPotionSelector().selectItemToBuy(shopItemBeans, gameStatus);
		} else {
			Random random = new Random();
			ShopItemBean shopItemBean = shopItemBeans[random.nextInt(shopItemBeans.length)];
			if (shopItemBean.getCost() <= gameStatus.getGold()) {
				return Optional.of(shopItemBean);
			}
			return Optional.empty();
		}
	}

}
