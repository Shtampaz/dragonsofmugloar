package lt.bigbank.dragonsofmugloar.selectors.shopitem;

import java.util.Optional;

import lt.bigbank.dragonsofmugloar.beans.BaseStatusBean;
import lt.bigbank.dragonsofmugloar.beans.ShopItemBean;

/**
 * Interface for a class, that should select an item from available items in a
 * shop (by implementing its own strategy) and buy it if enough gold is
 * available
 */
public interface ShopItemSelector {

	Optional<ShopItemBean> selectItemToBuy(ShopItemBean[] shopItemBeans, BaseStatusBean gameStatus);

}
