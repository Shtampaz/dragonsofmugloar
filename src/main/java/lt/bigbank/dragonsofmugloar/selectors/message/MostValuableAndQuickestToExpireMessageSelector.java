package lt.bigbank.dragonsofmugloar.selectors.message;

import java.util.List;
import java.util.Optional;

import lt.bigbank.dragonsofmugloar.beans.MessageBean;

public class MostValuableAndQuickestToExpireMessageSelector implements MessageSelector {

	@Override
	public MessageBean selectMessageToSolve(List<MessageBean> messages) {
		Optional<MessageBean> mostValuableMessageOptional = messages.stream().max((m1, m2) -> {
			if (m1.getReward() > m2.getReward()) {
				return 1;
			} else if (m1.getReward() < m2.getReward()) {
				return -1;
			} else if (m1.getExpiresIn() > m2.getExpiresIn()) {
				return -1;
			} else if (m1.getExpiresIn() < m2.getExpiresIn()) {
				return 1;
			}
			return 0;
		});

		return mostValuableMessageOptional.get();
	}

}
