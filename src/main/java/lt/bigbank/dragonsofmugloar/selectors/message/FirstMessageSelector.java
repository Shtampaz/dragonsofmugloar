package lt.bigbank.dragonsofmugloar.selectors.message;

import java.util.List;

import lt.bigbank.dragonsofmugloar.beans.MessageBean;

public class FirstMessageSelector implements MessageSelector {

	@Override
	public MessageBean selectMessageToSolve(List<MessageBean> messages) {
		return messages.get(0);
	}

}
