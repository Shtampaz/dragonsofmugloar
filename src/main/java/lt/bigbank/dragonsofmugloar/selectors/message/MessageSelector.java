package lt.bigbank.dragonsofmugloar.selectors.message;

import java.util.List;

import lt.bigbank.dragonsofmugloar.beans.MessageBean;

/**
 * Interface for a class, that should select a message from a list (by
 * implementing its own strategy) which should be solved
 */
public interface MessageSelector {

	/**
	 * 
	 * @param messages - expected not to be blank (not null and contains at least 1
	 *                 element)
	 * @return
	 */
	MessageBean selectMessageToSolve(List<MessageBean> messages);

}
