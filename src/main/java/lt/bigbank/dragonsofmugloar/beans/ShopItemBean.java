package lt.bigbank.dragonsofmugloar.beans;

public class ShopItemBean {

	private String id;

	private String name;

	private int cost;

	@Override
	public String toString() {
		return new StringBuilder("Item name: ").append(this.getName()).append(", item cost: ").append(this.getCost())
				.append(", item id: ").append(this.getId()).toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

}
