package lt.bigbank.dragonsofmugloar.beans;

public class GameBean extends ScoreStatusBean {

	private String gameId;

	private int level;

	@Override
	public String toString() {
		return new StringBuilder("Game started. Game id: ").append(this.getGameId()).append(". Initial dragon level: ")
				.append(this.getLevel()).append(". ").append(super.toString()).toString();
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

}
