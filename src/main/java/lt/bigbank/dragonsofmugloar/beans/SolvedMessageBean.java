package lt.bigbank.dragonsofmugloar.beans;

public class SolvedMessageBean extends ScoreStatusBean {

	private boolean success;

	private String message;

	@Override
	public String toString() {
		return new StringBuilder("Message solving successfull: ").append(isSuccess()).append(", message: ")
				.append(this.getMessage()).append(". ").append(super.toString()).toString();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
