package lt.bigbank.dragonsofmugloar.beans;

public class MessageBean {

	private String adId;

	private String message;

	private int reward;

	private int expiresIn;

	@Override
	public String toString() {
		return new StringBuilder("Message id: ").append(this.getAdId()).append(", message: ").append(this.getMessage())
				.append(", reward: ").append(this.getReward()).append(", expires in: ").append(this.getExpiresIn())
				.toString();
	}

	public String getAdId() {
		return adId;
	}

	public void setAdId(String adId) {
		this.adId = adId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getReward() {
		return reward;
	}

	public void setReward(int reward) {
		this.reward = reward;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

}
