package lt.bigbank.dragonsofmugloar.beans;

public class ScoreStatusBean extends BaseStatusBean {

	private int score;

	private int highScore;

	@Override
	public String toString() {
		return new StringBuilder("Current score: ").append(this.getScore()).append(", highscore: ")
				.append(this.getHighScore()).append(". ").append(super.toString()).toString();
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getHighScore() {
		return highScore;
	}

	public void setHighScore(int highScore) {
		this.highScore = highScore;
	}

}
