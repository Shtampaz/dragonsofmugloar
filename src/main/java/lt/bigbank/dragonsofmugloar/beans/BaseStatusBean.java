package lt.bigbank.dragonsofmugloar.beans;

public class BaseStatusBean {

	private int lives;

	private int gold;

	private int turn;

	@Override
	public String toString() {
		return new StringBuilder("Lives left: ").append(this.getLives()).append(", gold left: ").append(this.getGold())
				.append(", current turn: ").append(this.getTurn()).toString();
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

}
