package lt.bigbank.dragonsofmugloar.beans;

public class PurchasedItemBean extends BaseStatusBean {

	private String shoppingSuccess;

	private String level;

	@Override
	public String toString() {
		return new StringBuilder("Shopping successfull: ").append(this.getShoppingSuccess()).append(". Dragon level: ")
				.append(this.getLevel()).append(". ").append(super.toString()).toString();
	}

	public String getShoppingSuccess() {
		return shoppingSuccess;
	}

	public void setShoppingSuccess(String shoppingSuccess) {
		this.shoppingSuccess = shoppingSuccess;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

}
