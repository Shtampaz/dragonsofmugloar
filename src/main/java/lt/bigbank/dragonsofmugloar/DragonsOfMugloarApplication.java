package lt.bigbank.dragonsofmugloar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lt.bigbank.dragonsofmugloar.services.GameService;

@SpringBootApplication
public class DragonsOfMugloarApplication implements CommandLineRunner {

	private GameService game;

	@Autowired
	public DragonsOfMugloarApplication(GameService game) {
		this.game = game;
	}

	public static void main(String[] args) {
		SpringApplication.run(DragonsOfMugloarApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		game.play();
	}

}
