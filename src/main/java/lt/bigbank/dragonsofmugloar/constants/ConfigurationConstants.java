package lt.bigbank.dragonsofmugloar.constants;

public interface ConfigurationConstants {

	String MOST_VALUABLE_AND_QUICKEST_TO_EXPIRE_MESSAGE_SELECTOR = "MostValuableAndQuickestToExpireMessageSelector";
	String FIRST_MESSAGE_SELECTOR = "FirstMessageSelector";

	String HEALING_POTION_SELECTOR = "HealingPotionSelector";
	String SMART_RANDOM_SELECTOR = "SmartRandomSelector";

}
