package lt.bigbank.dragonsofmugloar.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import lt.bigbank.dragonsofmugloar.constants.ConfigurationConstants;
import lt.bigbank.dragonsofmugloar.selectors.message.FirstMessageSelector;
import lt.bigbank.dragonsofmugloar.selectors.message.MessageSelector;
import lt.bigbank.dragonsofmugloar.selectors.message.MostValuableAndQuickestToExpireMessageSelector;
import lt.bigbank.dragonsofmugloar.selectors.shopitem.HealingPotionSelector;
import lt.bigbank.dragonsofmugloar.selectors.shopitem.ShopItemSelector;
import lt.bigbank.dragonsofmugloar.selectors.shopitem.SmartRandomSelector;

@Configuration
public class BeansConfiguration {

	@Bean
	public RestTemplate restTemplate(@Value("${dragonsApi.baseUrl}") String url, RestTemplateBuilder builder) {
		return builder.rootUri(url).build();
	}

	@Bean
	public MessageSelector messageSelector(@Value("${messageSelector}") String messageSelector) {
		if (messageSelector != null) {
			switch (messageSelector) {
			case ConfigurationConstants.MOST_VALUABLE_AND_QUICKEST_TO_EXPIRE_MESSAGE_SELECTOR:
				return new MostValuableAndQuickestToExpireMessageSelector();
			case ConfigurationConstants.FIRST_MESSAGE_SELECTOR:
				return new FirstMessageSelector();
			}
		}
		return new FirstMessageSelector();
	}

	@Bean
	public ShopItemSelector shopItemSelector(@Value("${shopItemSelector}") String shopItemSelector) {
		if (shopItemSelector != null) {
			switch (shopItemSelector) {
			case ConfigurationConstants.HEALING_POTION_SELECTOR:
				return new HealingPotionSelector();
			case ConfigurationConstants.SMART_RANDOM_SELECTOR:
				return new SmartRandomSelector();
			}
		}
		return new HealingPotionSelector();
	}

}
