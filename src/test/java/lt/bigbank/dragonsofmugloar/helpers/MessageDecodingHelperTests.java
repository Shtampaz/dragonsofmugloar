package lt.bigbank.dragonsofmugloar.helpers;

import org.junit.Assert;
import org.junit.Test;

import lt.bigbank.dragonsofmugloar.beans.MessageBean;

public class MessageDecodingHelperTests {

	@Test
	public void shouldDecodeBase64Message() {
		MessageBean message = new MessageBean();
		message.setAdId("ckd1QnZCTU8=");
		message.setMessage(
				"SW52ZXN0aWdhdGUgQXJqYW4gU2ltcHNvbiBhbmQgZmluZCBvdXQgdGhlaXIgcmVsYXRpb24gdG8gdGhlIG1hZ2ljIGNoYXJpb3Qu");
		MessageBean decodedMessage = MessageDecodingHelper.decodeMessage(message);
		Assert.assertEquals("rGuBvBMO", decodedMessage.getAdId());
		Assert.assertEquals("Investigate Arjan Simpson and find out their relation to the magic chariot.",
				decodedMessage.getMessage());
	}

	@Test
	public void shouldNotDecodeRegularMessage() {
		MessageBean message = new MessageBean();
		message.setAdId("adid");
		message.setMessage("message");
		MessageBean decodedMessage = MessageDecodingHelper.decodeMessage(message);
		Assert.assertEquals("adid", decodedMessage.getAdId());
		Assert.assertEquals("message", decodedMessage.getMessage());
	}

}
