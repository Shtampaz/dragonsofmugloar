package lt.bigbank.dragonsofmugloar.selectors.message;

import java.util.ArrayList;
import java.util.List;

import lt.bigbank.dragonsofmugloar.beans.MessageBean;

public class MessageSelectorTestsHelper {

	public final static String FIRST_MESSAGE_ID = "1";
	public final static String MOST_VALUABLE_BUT_NOT_QUICKEST_TO_EXPIRE_MESSAGE_ID = "2";
	public final static String MOST_VALUABLE_AND_QUICKEST_TO_EXPIRE_MESSAGE_ID = "3";

	public static List<MessageBean> getTestMessages() {
		List<MessageBean> messages = new ArrayList<>();

		MessageBean message1 = new MessageBean();
		message1.setAdId(FIRST_MESSAGE_ID);
		message1.setExpiresIn(5);
		message1.setReward(10);
		messages.add(message1);

		MessageBean message2 = new MessageBean();
		message2.setAdId(MOST_VALUABLE_BUT_NOT_QUICKEST_TO_EXPIRE_MESSAGE_ID);
		message2.setExpiresIn(5);
		message2.setReward(100);
		messages.add(message2);

		MessageBean message3 = new MessageBean();
		message3.setAdId(MOST_VALUABLE_AND_QUICKEST_TO_EXPIRE_MESSAGE_ID);
		message3.setExpiresIn(2);
		message3.setReward(100);
		messages.add(message3);

		return messages;
	}

}
