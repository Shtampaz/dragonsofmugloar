package lt.bigbank.dragonsofmugloar.selectors.message;

import org.junit.Assert;
import org.junit.Test;

import lt.bigbank.dragonsofmugloar.beans.MessageBean;

public class MostValuableAndQuickestToExpireMessageSelectorTests {

	@Test
	public void shouldGetMostValuableAndQuickestToExpireMessage() {
		MessageSelector messageSelector = new MostValuableAndQuickestToExpireMessageSelector();
		MessageBean messageToSolve = messageSelector.selectMessageToSolve(MessageSelectorTestsHelper.getTestMessages());
		Assert.assertEquals(MessageSelectorTestsHelper.MOST_VALUABLE_AND_QUICKEST_TO_EXPIRE_MESSAGE_ID,
				messageToSolve.getAdId());
	}

}
