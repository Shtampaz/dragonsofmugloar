package lt.bigbank.dragonsofmugloar.selectors.message;

import org.junit.Assert;
import org.junit.Test;

import lt.bigbank.dragonsofmugloar.beans.MessageBean;

public class FirstMessageSelectorTests {

	@Test
	public void shouldGetFirstMessage() {
		MessageSelector messageSelector = new FirstMessageSelector();
		MessageBean messageToSolve = messageSelector.selectMessageToSolve(MessageSelectorTestsHelper.getTestMessages());
		Assert.assertEquals(MessageSelectorTestsHelper.FIRST_MESSAGE_ID, messageToSolve.getAdId());
	}

}
