package lt.bigbank.dragonsofmugloar.selectors.shopitem;

import org.junit.Assert;
import org.junit.Test;

import lt.bigbank.dragonsofmugloar.beans.BaseStatusBean;
import lt.bigbank.dragonsofmugloar.beans.ShopItemBean;
import lt.bigbank.dragonsofmugloar.constants.ShoppingItems;
import lt.bigbank.dragonsofmugloar.selectors.shopitem.ShopItemSelector;
import lt.bigbank.dragonsofmugloar.selectors.shopitem.SmartRandomSelector;

public class SmartRandomSelectorTests {

	private final ShopItemSelector selector = new SmartRandomSelector();

	private final ShopItemBean[] shopItemBeans = new ShopItemBean[2];

	public SmartRandomSelectorTests() {
		ShopItemBean healingPotion = new ShopItemBean();
		healingPotion.setId(ShoppingItems.HEALING_POTION);
		healingPotion.setCost(50);
		shopItemBeans[0] = healingPotion;

		ShopItemBean otherItem = new ShopItemBean();
		otherItem.setId("OtherItem");
		otherItem.setCost(100);
		shopItemBeans[1] = otherItem;
	}

	@Test
	public void shouldSelectHealingPotion() {
		BaseStatusBean baseStatus = new BaseStatusBean();
		baseStatus.setLives(1);
		baseStatus.setGold(100);
		Assert.assertEquals(ShoppingItems.HEALING_POTION,
				this.selector.selectItemToBuy(this.shopItemBeans, baseStatus).get().getId());
	}

	@Test
	public void shouldSelectAnyItem() {
		BaseStatusBean baseStatus = new BaseStatusBean();
		baseStatus.setLives(10);
		baseStatus.setGold(100);
		Assert.assertTrue(this.selector.selectItemToBuy(this.shopItemBeans, baseStatus).isPresent());
	}

	@Test
	public void shouldSelectNoItem() {
		BaseStatusBean baseStatus = new BaseStatusBean();
		baseStatus.setLives(10);
		baseStatus.setGold(0);

		Assert.assertFalse(this.selector.selectItemToBuy(this.shopItemBeans, baseStatus).isPresent());

		baseStatus.setLives(1);
		Assert.assertFalse(this.selector.selectItemToBuy(this.shopItemBeans, baseStatus).isPresent());
	}

}
