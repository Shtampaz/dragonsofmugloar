package lt.bigbank.dragonsofmugloar.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.web.client.RestTemplate;

import lt.bigbank.dragonsofmugloar.DragonsOfMugloarApplication;
import lt.bigbank.dragonsofmugloar.constants.ShoppingItems;
import lt.bigbank.dragonsofmugloar.services.GameService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DragonsOfMugloarApplication.class, initializers = ConfigFileApplicationContextInitializer.class)
public class DragonsofmugloarIntegrationTests {

	@Value("${dragonsApi.baseUrl}")
	private String baseUrl;

	@Value("${dragonsApi.gameStartPath}")
	private String gameStartPath;

	@Value("${dragonsApi.messagesPath}")
	private String messagesPath;

	@Value("${dragonsApi.solvingPath}")
	private String solvingPath;

	@Value("${dragonsApi.shopItemsPath}")
	private String shopItemsPath;

	@Value("${dragonsApi.shopItemPurchasePath}")
	private String shopItemPurchasePath;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private GameService game;

	private MockRestServiceServer mockServer;

	private String gameStartResponse = "{\"gameId\":\"V0y001Dd\",\"lives\":3,\"gold\":0,\"level\":0,\"score\":0,\"highScore\":4321,\"turn\":0}";
	private String gameId = "V0y001Dd";
	private String messagesResponse = "[{ \"adId\": \"SG77jMkB\", \"message\": \"Create an advertisement campaign for Ga\u00EBtane Whittemore to promote their squirrel based business\", \"reward\": 33, \"expiresIn\": 7,\"probability\": \"Walk in the park\"}]";
	private String messageId = "SG77jMkB";
	private String solveResponse = "{\"success\":true,\"lives\":3,\"gold\":50,\"score\":33,\"highScore\":4321,\"turn\":1,\"message\":\"You successfully solved the mission!\"}";
	private String shopResponse = "[ { \"id\": \"hpot\", \"name\": \"Healing potion\", \"cost\": 50 } ]";
	private String purchaseResponse = "{\"shoppingSuccess\":false,\"gold\":33,\"lives\":3,\"level\":0,\"turn\":2}";

	@Before
	public void init() {
		this.mockServer = MockRestServiceServer.createServer(this.restTemplate);
	}

	@Test
	public void shouldPlayAGame() {
		this.addExpectationWithSuccessfulResponseForMockServer(
				this.baseUrl.substring(0, this.baseUrl.length() - 1) + this.gameStartPath, this.gameStartResponse);
		this.addMessagesRetrievalExpectationForMockServer();
		this.addExpectationWithSuccessfulResponseForMockServer(
				this.baseUrl + String.format(this.solvingPath, this.gameId, this.messageId), this.solveResponse);
		this.addExpectationWithSuccessfulResponseForMockServer(
				this.baseUrl + String.format(this.shopItemsPath, this.gameId), this.shopResponse);
		this.addExpectationWithSuccessfulResponseForMockServer(
				this.baseUrl + String.format(this.shopItemPurchasePath, this.gameId, ShoppingItems.HEALING_POTION),
				this.purchaseResponse);
		this.addMessagesRetrievalExpectationForMockServer();
		this.mockServer
				.expect(MockRestRequestMatchers
						.requestTo(this.baseUrl + String.format(this.solvingPath, this.gameId, this.messageId)))
				.andRespond(MockRestResponseCreators.withStatus(HttpStatus.BAD_REQUEST));
		this.addMessagesRetrievalExpectationForMockServer();

		this.game.play();
	}

	private void addMessagesRetrievalExpectationForMockServer() {
		this.addExpectationWithSuccessfulResponseForMockServer(
				this.baseUrl + String.format(this.messagesPath, this.gameId), this.messagesResponse);
	}

	private void addExpectationWithSuccessfulResponseForMockServer(String requestUrl, String response) {
		this.mockServer.expect(MockRestRequestMatchers.requestTo(requestUrl))
				.andRespond(MockRestResponseCreators.withSuccess(response, MediaType.APPLICATION_JSON));
	}

}
