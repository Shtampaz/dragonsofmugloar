# DragonsOfMugloar

Program playing Dragons Of Mugloar game (Bigbank homework)

URL of game API is configurable in application.properties

*	App decides which message (ad) needs to be solved by checking messageSelector property in application.properties and initializing appropriate MessageSelector. Available values:
	*	MostValuableAndQuickestToExpireMessageSelector - selects the most valuable ad which has the lowest expiry value (preferred)
	*	FirstMessageSelector - selects first message from the list
	
*	App decides which item to buy from the shop by checking shopItemSelector property in application.properties and initializing appropriate ShopItemSelector. Available values:
	*	HealingPotionSelector - always buys a healing potion if there is enough gold
	*	SmartRandomSelector - if dragon has less than 5 lives - buys a healing potion. Otherwise buys a random item from available items (of course, if there's enough gold)